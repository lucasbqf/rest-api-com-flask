from flask_restful import Resource
from models.site import SiteModel

class Sites(Resource):
    def get(self):
        return {'sites': [site.json() for site in SiteModel.query.all()]}

class Site(Resource):
    def get(site, url):
        site = SiteModel.find_site(url)
        if site:
            return site.json()
        return {'message': 'site nao encontrado.'},404

    def post(self, url):
        if SiteModel.find_site(url):
            return{"message":"O site '{}' ja existe"},400
        site = SiteModel(url)
        try:
            site.save_site()
        except:
            return{'message': 'erro interno ao tentar salvar novo site'},500
        return site.json()

    def delete(self,url):
        site = SiteModel.find_site(url)
        if site:
            site.delete_site()
            return {'message':'site deletado.'},500
        return {'message': 'site nao encontrado'},404
