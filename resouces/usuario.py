from flask_restful import Resource, reqparse
from models.usuario import UserModel
from flask_jwt_extended import create_access_token, jwt_required, get_jwt
from werkzeug.security import safe_str_cmp
from blocklist import BLOCKLIST

atributos = reqparse.RequestParser()
atributos.add_argument('login', type=str,required=True, help="campo 'login' nao pode ser deixado em branco")
atributos.add_argument('senha', type=str,required=True, help="campo 'senha' nao pode ser deixado em branco")
atributos.add_argument('ativado', type=bool)

class User(Resource):

    def get(self,user_id):
        user= UserModel.find_user(user_id)
        if user:
            return user.json(),200
        return {'message': 'usuario nao encontrado'},404

    @jwt_required()
    def delete(self,user_id):
        user = UserModel.find_user(user_id)
        if user:
            try:
                user.delete_user()
                return {'message': 'usuario removido'},200
            except:
                return {'message':'DB delete error'},500
        return {'message': 'usuario nao encontrado'},404

class UserRegister(Resource):
    def post(self):
        dados = atributos.parse_args()
        if UserModel.find_by_login(dados['login']):
            return {'message':"usuario '{}' ja existe".format(dados['login'])}

        user = UserModel(**dados)
        user.ativado = False
        try:
            user.save_user()
            return{'message':'usuario criado com sucesso!'},201
        except:
            return {'message':'create user db internal error'},500

class UserLogin(Resource):
    @classmethod
    def post(cls):
        dados = atributos.parse_args()

        user = UserModel.find_by_login(dados['login'])
        if user and safe_str_cmp(user.senha,dados['senha']):
            if user.ativado:
                token_acesso = create_access_token(identity = user.user_id)
                return {'access_token': token_acesso},200
            return{'message':'usuario nao confirmado'},400
        return {'message':'usuario ou senha incorreto'},401


class UserLogout(Resource):
    @jwt_required()
    def post(self):
        jwt_id = get_jwt()['jti']
        BLOCKLIST.add(jwt_id)
        return {'message': 'Logout realizado com sucesso'}

class UserConfirm(Resource):
    #raiz+/confirmação/{user_id}
    @classmethod
    def get(cls,user_id):
        user = UserModel.find_user(user_id)
        if not user:
            return {'message':"user_id '{}' nao encontrado".format(user_id)},404
        user.ativado = True
        try:
            user.save_user()
            return {'message':"usuario '{}' confirmado".format(user_id)},200
        except:
            return{'message':"falha interna ao salvar Ativação"},500
